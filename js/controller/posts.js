app.controller('PostsCtrl', function ($scope, $rootScope, Post) {
    $rootScope.loading = true;
    $scope.posts = Post.find().then(function(posts){
        $scope.loading = false;
        $scope.posts = posts;
    }, function(msg){
            alert(msg);
    })
})
